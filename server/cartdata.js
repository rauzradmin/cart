let cartitems = [
    {
        id: 1,
        name: "Vivo V9",
        description: "awesome VIVO phone, duplicate of iphone x",
        image: "https://www.dropbox.com/s/0nc3c899xwg52pm/vivo-v9-1730-original-imaf3k6mrvdrhffm.jpeg?dl=0",
        price: 19000,
        quantity: 2  
    },
    {
        id: 2,
        name: "iPhone 6",
        description: "Coolest and cheapest iphone",
        image: "https://www.dropbox.com/s/wcnlmkkuy1v9otx/iphone6.jpeg?dl=0",
        price: 25000,
        quantity: 5
    },
    {
        id: 3,
        name: "iPhone 8",
        description: "Newest and expensive phone. Only if you have extra money.",
        image: "https://www.dropbox.com/s/qq986pt2o4fb3wg/iphone8.jpeg?dl=0",
        price: 80000,
        quantity: 5 
    },
    {
        id: 4,
        name: "iPhone X",
        description: "Most expensive phone ever made.",
        image: "https://www.dropbox.com/s/4pg6y7fmh47o71u/iphonex.jpeg?dl=0",
        price: 120000,
        quantity: 12  
    },
    {
        id: 5,
        name: "Moto E4 Plus",
        description: "Economical phone from Motorola",
        image: "https://www.dropbox.com/s/r5rh8qhv0zmry1b/motoe4plus.jpeg?dl=0",
        price: 13000,
        quantity: 4  
    },
    {
        id: 6,
        name: "Moto X4",
        description: "Moto's expensive phone for T Generation",
        image: "https://www.dropbox.com/s/773y88un7b0r2ws/motox4.jpeg?dl=0",
        price: 32000,
        quantity: 12 
    },
    {
        id: 7,
        name: "Redmi Note 5",
        description: "Another cheap phone from China",
        image: "https://www.dropbox.com/s/9jwmekme7k7vx29/redminote5.jpeg?dl=0",
        price: 33000,
        quantity: 1 
    },
    {
        id: 8,
        name: "Samsung S8",
        description: "Another day and another samsung phone",
        image: "https://www.dropbox.com/s/ws5cydwkzuxsk62/s8.jpeg?dl=0",
        price: 50000,
        quantity: 5 
    },
    {
        id: 9,
        name: "Samsung S9",
        description: "Latest and greatest from Samsung",
        image: "https://www.dropbox.com/s/f7hgl326zjndkpp/s9.jpeg?dl=0",
        price: 80000,
        quantity: 4
    }
    
]