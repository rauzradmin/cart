var CartItem = require("../models/cartitem")
var mongoose = require('mongoose');                     // mongoose for mongodb

module.exports = function(router) {

    router.route('/cartitems')
        .get(function(req,res){
            console.log('Finding cartitems')
            CartItem.find({}, function(err, items) {
                if (err) {
                    console.log(err)
                    res.status(500).send(err)
                } else {
                    console.log('sending items' + JSON.stringify(items, null , 2))
                    res.status(200).send(items)
                }
            })
        })

    router.route('/cartitems/:category')
        .get(function(req,res){
            console.log('Finding cartitems')
            let query = { category : req.params.category }
            if (req.params.category === "all") {
                query = {}
            }
            CartItem.find(query, function(err, items) {
                if (err) {
                    console.log(err)
                    res.status(500).send(err)
                } else {
                    console.log('sending items' + JSON.stringify(items, null , 2))
                    res.status(200).send(items)
                }
            })
        })



    router.route('/cartitem/:id')
            .get(function(req, res) {
                console.log('Looking for id: ' + req.params.id)
                CartItem.find({ _id : mongoose.Types.ObjectId(req.params.id)})
                        .exec((err, cartItem) => {
                            if (err) {
                                console.log(err)
                                res.status(500).send("fail")
                            } else {
                                console.log('Found item : ' + JSON.stringify(cartItem, null, 2))
                                res.status(200).send(cartItem[0])
                            }
                        })
            });

	router.route('/cartitem')
		.get(function(req, res) {
			res.send('caritems')
        })
        .post(function(req, res) {

            var cartItem = req.body;
            var newCartItem = new CartItem()

            newCartItem.name = cartItem.name
            newCartItem.itemtype = cartItem.itemtype
            newCartItem.image = cartItem.image
            newCartItem.description = cartItem.description
            newCartItem.price = cartItem.price
            newCartItem.quantity = cartItem.quantity
            newCartItem.category = cartItem.category

            console.log('Creating newItem: ' + JSON.stringify(newCartItem, null, 2))
            newCartItem.save(function(err) {
                if (err) {
                    console.log(JSON.stringify(err, null, 2))
                    console.log('fail')
                    res.status(500).send("Error" + err)
                } else {
                    res.send("success")
                }
            })
        })
        .put(function(req, res) {

            var newCartItem = req.body;

            CartItem.findById(mongoose.Types.ObjectId(newCartItem.id))
                    .exec((err, cartItem) => {
                        if (err) { console.log(err); res.status(500).send("fail"); return;}
                        Object.assign(cartItem, newCartItem);
                        cartItem.save(function(err) {
                            if (err) {
                                console.log(JSON.stringify(err, null, 2))
                                console.log('fail')
                                res.status(500).send("Error" + err)
                            } else {
                                res.send("success")
                            }
                        })
                    });
            
        });
}