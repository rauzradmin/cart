var Category = require("../models/category")
var mongoose = require('mongoose');                     // mongoose for mongodb

module.exports = function(router) {


    router.route('/categories/:parent')
        .get(function(req,res){
            console.log('Finding categories' + req.params.parent)

            let query = { parent: req.params.parent }
            // if (req.params.parent === "all") {
            //     query = {}
            // }
            Category.find(query , function(err, items) {
                if (err) {
                    console.log(err)
                    res.status(500).send(err)
                } else {
                    console.log('sending items' + JSON.stringify(items, null , 2))
                    res.status(200).send(items)
                }
            })
        })

        // router.route('/categories')
        // .get(function(req,res){
        //     console.log('Finding categories')
        //     Category.find({}, function(err, items) {
        //         if (err) {
        //             console.log(err)
        //             res.status(500).send(err)
        //         } else {
        //             console.log('sending items' + JSON.stringify(items, null , 2))
        //             res.status(200).send(items)
        //         }
        //     })
        // });
    

}