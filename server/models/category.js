var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CategorySchema = new Schema({
    name: { type: String, required : true },
    image: String,
    description: String,
    parent: String
});

module.exports = mongoose.model('Category', CategorySchema);