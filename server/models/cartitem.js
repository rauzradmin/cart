var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CartItemSchema = new Schema({
    
    name: { type: String, required : true },
    itemtype: { type: String, enum: ['lighting', 'furniture', 'other'], default: 'other'},
    image: String,
    description: String,
    price: Number,
    quantity: Number,
});

module.exports = mongoose.model('CartItem', CartItemSchema);