var express = require('express');
var app = express();
var bodyParser = require('body-parser');    // pull information from HTML POST (express4)
var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)
var session      = require('express-session');
var cookieParser = require('cookie-parser');
var path = require('path')

app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
app.use(bodyParser.json());                                     // parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(methodOverride());

var mongoose = require('mongoose');                     // mongoose for mongodb
mongoose.connect("mongodb://localhost/cart");
var db = mongoose.connection;

db.on('error', function(err) {
    console.log('ERROR')
});

db.once('open', function() {
    console.log('Database is connected!!')
});


   
app.use('/hello', function(req, res) {
    res.send('Helloworld');
})



app.use(function(req, res, next) { 
    res.header("Access-Control-Allow-Origin", "*"); 
    res.header("Access-Control-Allow-Methods", "GET,POST,OPTIONS,DELETE,PUT"); 
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept"); 
    next(); });
    
var router = express.Router();
router.use(function(req, res, next) {
    console.log('in router' );
    next();
  });
require('./controllers/cartController')(router)
require('./controllers/categoryController')(router)

app.use('/api', router);





var server = app.listen(8081, function () {

    var host = server.address().address
    var port = server.address().port
    console.log("vizkart app listening at http://%s:%s", host, port)
  
  })
