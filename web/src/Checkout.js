import React, { Component } from 'react';
import './App.css';
import {Navbar, Nav, NavDropdown, NavItem, MenuItem, FormGroup, FormControl, Button, Modal} from 'react-bootstrap'



export default class Checkout extends React.Component {
    constructor(props, context) {
      super(props, context);
  
      this.handleHide = this.handleHide.bind(this);
  
      this.state = {
        show: false
      };
    }
  
    handleHide() {
      this.setState({ show: false });
    }
    render() {

        console.log('Rendering heckout ' + JSON.stringify(this.props))
      const total = this.props.items.reduce((acc , item) => { return acc + item.price }, 0)


      return (
        <div className="modal-container" style={{textAlign: "right", marginRight: "20px"}}>
          <Button
            bsStyle="primary"
            bsSize="large"
            
            onClick={() => this.setState({ show: true })}
          >
            Total {total} INR
          </Button>
  
          <Modal
            show={this.state.show}
            onHide={this.handleHide}
            container={this}
            aria-labelledby="contained-modal-title"
          >
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title">
                Checkout
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              {this.props.items.map((item) => {
                  return <div>
                      {item.name} = {item.price}
                  </div>
              })}
              {"-----------------------------------"}
              <div>Total: {total}</div>
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={this.handleHide}>Close</Button>
            </Modal.Footer>
          </Modal>
        </div>
      );
    }
  }

