import React, { Component } from 'react';
import './App.css';
import {Navbar, Nav, NavDropdown, NavItem, MenuItem, FormGroup, FormControl, Button} from 'react-bootstrap'


export default class CartItem extends Component {

    constructor(props) {
      super(props);
  
      console.log('this.data' + JSON.stringify(this.props.data, null, 2))
      this.state = {
        id: this.props.data._id,
        name : this.props.data.name,
        description : this.props.data.description,
        price : this.props.data.price,
        quantity : this.props.data.quantity,
        image: this.props.data.image
      }
  
      this.onBuy = (e) => {
        e.preventDefault();
        this.props.onBuy(this.props.data)
        this.setState({
                         quantity : this.state.quantity - 1
                      })
      }
    }
  
    
  
    render() {

      console.log('Rendering cart item: ' + JSON.stringify(this.props.data.name))
      return (
        <div className="col-sm-3" style={{marginTop: "20px", height: "380px"}}>
          <div  className="thumbnail text-center card1">
            <a href="#" onClick={() => this.props.editItem(this.state.id)}  style={{position: "absolute", right: "20px" , top: "10px"}}>
              <i class="fa fa-pencil" aria-hidden="true" style={{fontSize: "12px"}}></i>
            </a>
            <a href="#" onClick={() => this.props.deleteItem(this.state.id)} style={{position: "absolute", right: "40px" , top: "10px"}}>
              <i class="fa fa-times" aria-hidden="true" style={{fontSize: "12px"}}></i>
            </a>
            <img src={this.props.data.image} style={{ height : "200px"}}/>
            <h3>{this.props.data.name}</h3>
            <span > {this.props.data.price} <i className="fa fa-usd" aria-hidden="true"></i></span>
            Quantity {this.props.data.quantity}
            <p style={{marginTop:"10px", height: "20px", overflow: "hidden"}}>
                {this.props.data.description}
            </p>
            <p>
                <a href="#" className="btn btn-primary" onClick={this.onBuy} style={{marginRight: "10px"}}>Buy</a>
                <a href="#" className="btn btn-default">Add to wish list</a>
            </p>
          </div>  
        </div>
        
      )
    }
  }
  
  
  