import React, { Component } from 'react';
import './App.css';
import {Navbar, Nav, NavDropdown, NavItem, MenuItem, FormGroup, FormControl, Button, Modal, OverlayTrigger, Popover, Tooltip, Checkbox, Radio, ControlLabel, HelpBlock} from 'react-bootstrap'

function FieldGroup({ id, label, help, ...props }) {
    return (
      <FormGroup controlId={id}>
        <ControlLabel>{label}</ControlLabel>
        <FormControl {...props} />
        {help && <HelpBlock>{help}</HelpBlock>}
      </FormGroup>
    );
  }

class CreateCartItem extends React.Component{

    constructor(props) {

        super(props)

        this.state = {
            show : true,
            name: this.props.editItem.name,
            description: this.props.editItem.description,
            image: this.props.editItem.image,
            price: this.props.editItem.price,
            quantity: this.props.editItem.quantity,
            id: this.props.editItem._id
        }

        this.handleClose = () => {
            this.setState({
                show: false
            });
            this.props.onHide()
        }

        this.handleCreate = () => {
            this.setState({
                show: false
            });
            this.props.onCreate({
                id: this.state.id,
                name: this.state.name,
                description: this.state.description,
                image: this.state.image,
                price: this.state.price,
                quantity: this.state.quantity
            })
        }

        this.handleChangeName = (event) => {
            this.setState({
                name : event.target.value
            })
        }

        this.handleDescriptionChange = (event) => {
            this.setState({
                description : event.target.value
            })
        }

        this.handleImageChange = (event) => {
            this.setState({
                image : event.target.value
            })
        }

        this.handlePriceChange = (event) => {
            this.setState({
                price : event.target.value
            })
        }

        this.handleQuantityChange = (event) => {
            this.setState({
                quantity : event.target.value
            })
        }


    }
    

    render() {

        const popover = (
            <Popover id="modal-popover" title="popover">
              very popover. such engagement
            </Popover>
          );
        const tooltip = <Tooltip id="modal-tooltip">wow.</Tooltip>;
        let buttonLabel = "create"
        if (this.state.id) {
            buttonLabel = "Update" 
        }
          

        return (
            <div>
                
                <Modal show={this.state.show} onHide={this.handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Create Cart Item</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                    <form>

                        <FieldGroup
                            id="itemName"
                            type="text"
                            label="Item Name"
                            placeholder="Enter item name"
                            value={this.state.name}
                            onChange={this.handleChangeName}
                        />

                        <FieldGroup
                            id="itemDescription"
                            type="text"
                            label="Item Description"
                            placeholder="Enter item description"
                            value={this.state.description}
                            onChange={this.handleDescriptionChange}
                        />

                        <FieldGroup
                            id="itemPrice"
                            type="number"
                            label="Price"
                            placeholder="Enter price"
                            value={this.state.price}
                            onChange={this.handlePriceChange}
                        />

                        <FieldGroup
                            id="itemQuantity"
                            type="number"
                            label="Item Quantity"
                            placeholder="Enter item quantity"
                            value={this.state.quantity}
                            onChange={this.handleQuantityChange}
                        />

                         <FieldGroup
                            id="itemImage"
                            type="text"
                            label="Enter Image Link"
                            placeholder="Enter image link"
                            value={this.state.image}
                            onChange={this.handleImageChange}
                        />

                    </form>
                    </Modal.Body>

                    <Modal.Footer>
                        <Button onClick={this.handleCreate}>{buttonLabel}</Button>
                    </Modal.Footer>

                </Modal>
            </div>
        )
    }

}

export default CreateCartItem;
