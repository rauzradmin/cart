import $ from 'jquery';

import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {Navbar, Nav, NavDropdown, NavItem, MenuItem, FormGroup, FormControl, Button} from 'react-bootstrap'
import Categories from './Categories'
import Cart from './Cart'
import CreateCartItem from './CreateCartItem'
import AppNavbar from './Navbar'
import AppCarousel from './Carousel'
import Checkout from './Checkout'
window.jQuery = $;

class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      categories: [],
      cartItems : [],
      searchString: "",
      createItem : false,
      editItem: {},
      itemsBought: [],
      itemsWished: []

    }

    this.fetchCartItems = () => {

      let that = this
      fetch('http://localhost:8081/api/cartitems')
        .then( (response) => {
          return response.json();
        })
        .then((data) => {
          console.log(data);
          that.cartItems = data
          that.setState((prev, props) => {
            return { cartItems : data } 
          })
        })
        .catch((err) => {
          console.log("ERROR: " + JSON.stringify(err));
        });
        
    }

    this.onSelect = (category) => {
      let that = this

      fetch('http://localhost:8081/api/categories/' + category)
        .then( (response) => {
          return response.json();
        })
        .then((data) => {
          console.log('Categories loaded' + JSON.stringify(data, null, 2))
          that.setState({
            categories: data
          })
        })
        .catch((err) => {
          console.log("ERROR: " + JSON.stringify(err));
        });



      fetch('http://localhost:8081/api/cartitems/' + category)
        .then( (response) => {
          return response.json();
        })
        .then((data) => {
          that.setState({
            cartItems: data
          })
        })
        .catch((err) => {
          console.log("ERROR: " + JSON.stringify(err));
        });
    }

    this.createItem = () => {
      console.log('Crete Item')
      this.setState({
        createItem : true
      })
    }

    this.hideCreateItem = (newItem) => {

      this.setState({
        createItem : false
      })
    }

    this.editItem = (id) => {
      alert('Edit Item ' + id)

      let that = this
      fetch('http://localhost:8081/api/cartitem/' + id)
        .then( (response) => {
          return response.json();
        })
        .then((data) => {
          that.setState({
            createItem : true,
            editItem: data
          })
        })
        .catch((err) => {
          console.log("ERROR: " + JSON.stringify(err));
        });
    }

    this.deleteItem = (id) => {
      alert('Delete Item ' + id)
    }

    this.onBuy = (cartitem) => {
      console.log('BOught ' + JSON.stringify(cartitem, null, 2))
      this.setState((prev, props) => {
        return {itemsBought : [...prev.itemsBought, cartitem]}
      })

      console.log('ITEMS Bought' + JSON.stringify(this.state.itemsBought))

    }

    this.hideAndCreateItem = (newItem) => {

      if (newItem) {
        console.log('Create Item: ' + JSON.stringify(newItem))

        let that = this
        if (newItem.id) {
          $.ajax({
            url: "http://localhost:8081/api/cartitem",
            type: 'PUT',
            data: newItem,
            success: function(result) {
              that.fetchCartItems();
              console.log(result)
            }});

        } else {
          $.post("http://localhost:8081/api/cartitem",
          {
             "name" : newItem.name, 
             "description": newItem.description, 
             "price": newItem.price, 
             "quantity": newItem.quantity, 
             "image" : newItem.image
            },
            function(data) {
              that.fetchCartItems();
              console.log(data)
            } )
        }
            
      }

      this.setState({
        createItem : false
      })
    }

    this.search = (searchString) => {
     
     this.setState({
       searchString: searchString,
       cartItems: this.cartItems.filter((cartItem) => {
         return cartItem.name.toLowerCase().indexOf(searchString.toLowerCase()) >= 0 || cartItem.description.toLowerCase().indexOf(searchString.toLowerCase()) >= 0
       })
     })
    }

    this.fetchCategories = (name) =>{

    }

    this.onSelectCategory = (name) => {

    }
  }

  componentWillMount(){
    this.fetchCartItems()
  }

  render(){
    return (
      <div className="containerf-fluid">
        
        {this.state.createItem &&
          <CreateCartItem onHide={this.hideCreateItem} onCreate={this.hideAndCreateItem} editItem={this.state.editItem}/>
        }
        
        <AppNavbar onCreate={this.createItem} onSearch={this.search} onSelect={this.onSelect}/>
        <Checkout items={this.state.itemsBought} />
        {/* <AppCarousel /> */}
        {this.state.categories.length > 0 &&
          <Categories items={this.state.categories} onSelect={this.onSelect}/>
        }
        
        {this.state.categories.length <= 0 && this.state.cartItems.length >0  &&
          <Cart cartItems={this.state.cartItems} editItem={this.editItem} deleteItem={this.deleteItem} onBuy={this.onBuy}/> 
        }

      </div>
    )
  }
}




class User extends Component {

  render() {
    return(
      <h4>{this.props.name}</h4>
    )
  }
}

export default App;
