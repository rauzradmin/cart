
import React, { Component } from 'react';
import './App.css';
import {Navbar, Nav, NavDropdown, NavItem, MenuItem, FormGroup, FormControl, Button} from 'react-bootstrap';
import { Carousel } from 'react-responsive-carousel';

export default class AppCarousel extends Component {

    constructor(props) {
        super(props)

    }

    render(){
        return (
            <Carousel style={{height: "200px"}}>
                <div>
                    <img src="./images/1.jpeg" />
                    <p className="legend">Picture 1</p>
                </div>
                <div>
                    <img src="./images/2.jpeg" />
                    <p className="legend">Picture 2</p>
                </div>
                <div>
                    <img src="./images/3.jpeg" />
                    <p className="legend">Picture 3</p>
                </div>
            </Carousel>
        );
    }
}