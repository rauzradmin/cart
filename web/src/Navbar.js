import React, { Component } from 'react';
import './App.css';
import {Navbar, Nav, NavDropdown, NavItem, MenuItem, FormGroup, FormControl, Button} from 'react-bootstrap'

export default class AppNavbar extends Component {

    constructor(props) {
      super(props)
      this.state = {
        searchString : ""
      }
  
      this.handleChange = (e) => {
        this.props.onSearch(e.target.value)
        this.setState({
          searchString: e.target.value
        })
      }
  
    }
    render() {
        console.log(this.props)
      return(
        <Navbar inverse>
            <Navbar.Header>
                <Navbar.Brand>
                    <a href="#home" onClick={() => this.props.onSelect('all')}>MobKart</a>
                </Navbar.Brand>
                <Navbar.Toggle />
            </Navbar.Header>
            <Navbar.Collapse>
                

                <Navbar.Form pullRight>
                    <FormGroup>
                        <FormControl type="text" placeholder="Search" onChange={this.handleChange}/>
                        </FormGroup>{' '}
                    <Button type="button" onClick={this.props.onCreate}>Create</Button>
                </Navbar.Form>
                {this.state.searchString && 
                    <Navbar.Text pullRight>{this.state.searchString}...</Navbar.Text>
                }
                <Nav>
                    <NavItem eventKey={1} href="#" onClick={() => this.props.onSelect('ios')}>
                        IOS
                    </NavItem>
                    <NavItem eventKey={2} href="#" onClick={() => this.props.onSelect('android')}>
                        Android
                    </NavItem>
                </Nav>    
            </Navbar.Collapse>
        </Navbar>
      )
    }
  }
  