import React, { Component } from 'react';
import './App.css';
import {Navbar, Nav, NavDropdown, NavItem, MenuItem, FormGroup, FormControl, Button} from 'react-bootstrap';
import CategoryItem from './Category'

export default class Categories extends Component {
    constructor(props) {
      super(props)
    }
  
    render() {
  
      console.log(this.props.items);
      return (
        <div>
        { this.props.items.map((category) => {
          return <CategoryItem item={category} key={category.name} onSelect={this.props.onSelect} />
        })}
        </div>
      )
    }
  }