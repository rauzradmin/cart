import React, { Component } from 'react';
import './App.css';
import {Navbar, Nav, NavDropdown, NavItem, MenuItem, FormGroup, FormControl, Button, Carousel} from 'react-bootstrap'

export default class CategoryItem extends Component {

    constructor(props) {
      super(props)
    }
  
    render() {
      return (
        <div className="col-sm-3" style={{marginTop: "20px", height: "380px"}}>
            <div  className="thumbnail text-center card1" onClick={() => this.props.onSelect(this.props.item.name)}>
               
                <img src={this.props.item.image} style={{ height : "200px"}}/>
                <h3>{this.props.item.name}</h3>
                <p style={{marginTop:"10px", height: "20px", overflow: "hidden"}}>
                    {this.props.item.description}
                </p>
            </div>  
        </div>)
    }
  }
  