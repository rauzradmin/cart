import React, { Component } from 'react';
import './App.css';
import {Navbar, Nav, NavDropdown, NavItem, MenuItem, FormGroup, FormControl, Button} from 'react-bootstrap';
import CartItem from './CartItem'

export default class Cart extends Component {
    constructor(props) {
      super(props)
 
    }
 
    render() {
     // console.log('Render cart ites' + JSON.stringify(this.props.cartItems, null, 2))
      return (
        <div>
         { this.props.cartItems.map((item) => {
           console.log("CART ITEM" + JSON.stringify(item, null, 2))
           return <CartItem data={item} key={item._id} {...this.props}/>
         })}
         </div>
      );
    }
 }
 