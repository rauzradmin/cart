/*Method Invocation pattern */

let myObject = {
    val : 1, 
    myFunc : function() {
        this.val++;
        console.log(this.val);
    },
    myFunc2: function(){
        this.val++;
        this.val++;
        let that = this;

        //Function Invocation
        let myInnerFunc3 = function() {
            //'this' points to global here
            console.log(that.val);
        }
        myInnerFunc3();
    }
}

 myObject.myFunc();
 myObject.myFunc();
myObject.myFunc2();

/* Constructor Invocatoin */
let Quo = function(s) {
    this.status = s
}
Quo.prototype.getStatus = function() {
    return this.status;
}

let q = new Quo("constructor")
let q1 = new Quo("constructor1")
console.log(q.getStatus())
console.log(q1.getStatus())

//Modules



//this



//arrays



//closure



//Functional programming
 
